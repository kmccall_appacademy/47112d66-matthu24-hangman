class Hangman
  attr_reader :guesser, :referee, :board, :missed_guesses, :remaining_wrong_guesses
  #Pass options hash to game: Hangman.new(guesser: guesser, referee: referee).play
  def initialize(players)
    #players is an options hash
    @guesser = players[:guesser]
    @referee = players[:referee]
    @remaining_wrong_guesses = 10
  end

  def setup
    secret_length=@referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = [nil] * (secret_length)
    @missed_guesses = []
    print @board
    puts ""
  end

  def update_board(found_indices,guess)
    found_indices.each {|idx| @board[idx] = guess }
    @remaining_wrong_guesses -= 1 if found_indices == []
  end

  def update_missed_guesses(letter)
    @missed_guesses << letter
  end


  def take_turn
    #guess is a letter
    guess = @guesser.guess(@board)
    #check guess returns array of correct indices
    found_indices = @referee.check_guess(guess)
    update_missed_guesses(guess) if found_indices == []
    update_board(found_indices,guess)
    @guesser.display_board(@board,@missed_guesses) if @guesser.is_a?(HumanPlayer)
    @guesser.handle_response(guess,found_indices)
  end

  def won?
    @board.include?(nil) == false
  end

  def play
    setup
    until won? || @remaining_wrong_guesses == 0
      take_turn
      puts "#{@remaining_wrong_guesses} wrong guesses remaining."
    end
    puts "Word guessed correctly" if won?
    puts "Game over.  Too many guesses" if @remaining_wrong_guesses == 0
  end
end

class HumanPlayer
attr_reader :secret_word

  def guess(board)
    puts "input guess: "
    guess = $stdin.gets.chomp
  end

  def display_board(board,missed_guesses)
    puts "board:"
    print board
    puts ""
    puts "missed guesses:"
    print missed_guesses
    puts ""
  end

  def handle_response(guess, response)
    puts "Found #{guess} at positions #{response}"
  end

  def register_secret_length(length)
    puts "Secret is #{length} letters long"
  end

  def pick_secret_word
    puts "input secret word length: "
    secret_length = $stdin.gets.chomp
    secret_length.to_i
  end

  def check_guess(guess)
    found_indices = []
    puts "If guess is correct, enter the correct index for the letter.  If multiple indices, enter indices like so: 145. If incorrect letter, input none"
    correct_indices = $stdin.gets.chomp
    return found_indices if correct_indices == "none"

    correct_indices.each_char do |char|
      found_indices << char.to_i
    end
    found_indices
  end

end

#The computer player should read in a dictionary file
#(lib/dictionary.txt) and choose a word randomly
class ComputerPlayer
  attr_reader :dictionary, :secret_word, :candidate_words, :guessed_letters

  #CONSIDER THIS: for getting rid of extra space in computer guess
  #@dictionary = File.readlines("dictionary.txt").map(&:chomp)
  def initialize(dictionary=File.readlines("dictionary.txt").map(&:chomp))
    @dictionary = dictionary
    @guessed_letters = []
  end

  def pick_secret_word
    dictionary_length = @dictionary.length
    secret_word = @dictionary[rand(dictionary_length)]
    @secret_word = secret_word
    @secret_word.length
  end


  def check_guess(letter)
    found_indices = []
    @secret_word.each_char.with_index do |char,index|
      found_indices << index if letter == char
    end
    found_indices
  end

  #rejects words of wrong length from candidate_words
  def register_secret_length(length)
    @candidate_words = @dictionary.select{|word| word.length == length}
  end

  def guess(board)
    frequency = Hash.new(0)
    @candidate_words.each do |word|
      word.each_char do |char|
        frequency[char]+=1
      end
    end
    #frequency is an array of arrays, each subarray
    #is a letter with its frequency
    #sorted in descending frequency
    frequency = frequency.sort_by{|k,v|v}.reverse
    #go thru the most frequent letters
    frequency.each do |sub_array|
      if board.include?(sub_array[0]) == false && @guessed_letters.include?(sub_array[0]) == false
        @guessed_letters << sub_array[0]
        print sub_array[0]
        puts ""
        return sub_array[0]
      end
    end

  end

  def handle_response(guess, response_indices)
    @candidate_words.reject! do |word|
       should_reject = false
       word.split("").each_with_index do |char,index|
         if response_indices.include?(index) && char != guess
           should_reject = true
         elsif !response_indices.include?(index) && char == guess
           should_reject = true
         end
       end
       should_reject
     end
  end

end
